﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace szimul1
{
    public partial class Form1 : Form
    {
        SzimTer szimulacio = new SzimTer();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            szimulacio.inic(Convert.ToInt32(tbN.Text), 
                            Convert.ToInt32(tbK.Text), 
                            Convert.ToDouble(numericUpDown1.Value) / 100.0);

            dataGridView1.RowCount = Convert.ToInt32(tbK.Text);
            dataGridView1.ColumnCount = Convert.ToInt32(tbN.Text);
            for (int x=0; x< dataGridView1.ColumnCount; x++)
            {
                for (int y=0; y< dataGridView1.RowCount; y++)
                {
                    dataGridView1.Rows[y].Cells[x].Value = szimulacio.ter[y, x];
                } 
            }
        
            panel1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            szimulacio.SzimLepes();
            checkBox1.Text = szimulacio.ido.ToString();
            for (int x = 0; x < dataGridView1.ColumnCount; x++)
            {
                for (int y = 0; y < dataGridView1.RowCount; y++)
                {
                    dataGridView1.Rows[y].Cells[x].Value = szimulacio.ter[y, x];
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            timer1.Enabled = checkBox1.Checked;
        }

        private void bMentes_Click(object sender, EventArgs e)
        {
            checkBox1.Checked = false;
            timer1.Enabled = false;
            saveFileDialog1.ShowDialog();
        }

        private void bBetolt_Click(object sender, EventArgs e)
        {
            checkBox1.Checked = false;
            timer1.Enabled = false;
            openFileDialog1.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            szimulacio.mentes(saveFileDialog1.FileName);
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            szimulacio.betoltes(openFileDialog1.FileName);
            //kepernyo frissites
            checkBox1.Text = szimulacio.ido.ToString();
            for (int x = 0; x < dataGridView1.ColumnCount; x++)
            {
                for (int y = 0; y < dataGridView1.RowCount; y++)
                {
                    dataGridView1.Rows[y].Cells[x].Value = szimulacio.ter[y, x];
                }
            }
        }

        private void tbG_Scroll(object sender, EventArgs e)
        {
            szimulacio.G = tbG.Value;
        }
    }
    public class SzimTer
    {
        const char mol = 'X';
        const char ures = ' ';

        public int ido;
        public int n, k;
        double suruseg;
        public int G;
        public char[,] ter = new char[100, 100];
        public SzimTer() { ido = 0; }
        public void inic(int iN, int iK, double iSuruseg)
        {
            n = iN;
            k = iK;
            suruseg = iSuruseg;
            G = 0;
            Random r = new Random();
            for (int x = 0; x < n; x++)
            {
                for (int y = 0; y < k; y++)
                {
                    if (r.Next(100) < suruseg * 100)
                        ter[y, x] = mol;
                    else
                        ter[y, x] = ures;
                }
            }
        }

        int Szomszedszam(int y, int x)
        {
            int nr = 0;
            for (int iy=y-1; iy<=y+1; iy++)
            {
                for (int ix=x-1; ix<=x+1; ix++)
                {
                    if ((0<=iy && iy<k && 0<=ix && ix<n) && (ix!=x || iy!=y))
                        if (ter[iy,ix]==mol)
                        {
                            nr++;
                        }
                }
            }
            return nr;
        }
        public void SzimLepes()
        {
            ido++;
            Random r = new Random();
            //gázmodell_ egyszerű csere hely-szomszéd hely között
            //veletlenhely()
            int velx = r.Next(n);
            int vely = r.Next(k);

            //véletlen szomszéd választás
            int szx;
            int szy;
            do
            {
                szx = r.Next(3) - 1 + velx;
                szy = r.Next(3) - 1 + vely;
            } while (szx < 0 || szx >= n || szy < 0 || szy >= k);

            if (ter[vely,velx]==mol && ter[szy, szx] == ures && Szomszedszam(szy,szx)+(szy-vely)*G>=Szomszedszam(vely,velx))
            {
                //csere
                char c = ter[vely, velx];
                ter[vely, velx] = ter[szy, szx];
                ter[szy, szx] = c;
            }
        }
        public void mentes(string filename)
        {
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@filename))
            {
                file.WriteLine(ido);
                file.WriteLine(n);
                file.WriteLine(k);
                file.WriteLine(suruseg);
                file.WriteLine(G);
                for (int y=0; y<k; y++)
                {
                    string s = "";
                    for (int x = 0; x < n; x++)
                    {
                        s = s + ter[y, x];
                    }
                    file.WriteLine(s);
                }
            }

        }
        public void betoltes(string filename)
        {
            using (System.IO.StreamReader file =
                new System.IO.StreamReader(@filename))
            {
                string s;
                s = file.ReadLine(); 
                ido = Convert.ToInt32(s);
                s = file.ReadLine();
                n= Convert.ToInt32(s);
                s = file.ReadLine();
                k= Convert.ToInt32(s);
                s =file.ReadLine();
                suruseg = Convert.ToDouble(s);
                s = file.ReadLine();
                G = Convert.ToInt32(s);

                for (int y = 0; y < k; y++)
                {
                    s=file.ReadLine();
                    int x = 0;
                    foreach (char c in s)
                    {
                        ter[y, x]=c;
                        x++;
                    }
                }
            }

        }
    }

}
